chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    console.log("shatem is running");

    const tables = document.getElementsByTagName("nz-table");

    console.log("tables length = ", tables.length);
    const data = [];
    for (let iT in tables) {
        const table = tables[iT];
        if (!table.getElementsByTagName) continue;
        const tbodies = table.getElementsByTagName("tbody");
        for (let tbody of tbodies) {
            if (!tbody.getElementsByTagName) continue;
            const rows = tbody.getElementsByTagName("tr");
            let brandName = "";
            let brandNumber = "";
            let descH4 = "";
            let descP = "";
            console.log("rows.length = ", rows.length);
            for (let iR in rows) {
                const row = rows[iR];
                if (!row.getElementsByTagName) continue;
                const cells = row.getElementsByTagName("td");
                if (cells.length <= 1) continue;
                // console.log("row.innerText = ", row.innerText);
                console.log("cells.length = ", cells.length, cells);
                // brand_article +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                const brand_article = cells[0].getElementsByTagName("strong");
                console.log("brand_article.length = ", brand_article.length);
                let offset = 2;
                if (brand_article.length === 2) {
                    brandName = brand_article[0].innerText.trim();
                    brandNumber = brand_article[1].innerText.trim();
                    // description +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    // const rawDesc = cells[1].innerText.trim();
                    descH4 = cells[1].getElementsByTagName("h4");
                    descP = cells[1].getElementsByTagName("p");
                } else offset = 0;

                const dataRow = {};

                // const cells = iRR.getElementsByTagName("td");
                dataRow.brand = brandName;
                dataRow.number = brandNumber;
                dataRow.descH4 = descH4[0].innerText;
                dataRow.descP = descP[0].innerText;
                dataRow.descManagerComment = "";
                dataRow.store = cells[offset + 0].innerText === "" ? "UNDF" : cells[offset + 0].innerText;
                dataRow.supplier = "Шате-М";
                dataRow.avail = cells[offset + 1].innerText;
                dataRow.arriv = cells[offset + 2].innerText;
                if( dataRow.arriv && dataRow.arriv.length > 4 ){
                    dataRow.arriv += ".2020";
                }
                console.log( parseInt(dataRow.arriv) );
                dataRow.price = parseInt(
                    cells[offset + 3].innerText.split(",")[0].replace(/\s+/g, '')
                );
                data.push({ ...dataRow });
            }
        }
    }
    console.log(data);
    sendResponse(data);
    // const div = "<div>Heelog</div>";
    // div.textContent = "Hellog";
    // console.log( div )
    // tables[0].appendChild(div);
});
